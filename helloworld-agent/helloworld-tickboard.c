/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "helloworld-tickboard.h"

struct _HlwTickBoard
{
  HlwDBusTickBoardSkeleton parent;

  GSource *timeout_source;              /* owned */
  guint timeout_id;
};

static void hlw_tick_board_tick_board_iface_init (HlwDBusTickBoardIface *iface);

G_DEFINE_TYPE_WITH_CODE (HlwTickBoard, hlw_tick_board,
                         HLW_DBUS_TYPE_TICK_BOARD_SKELETON,
                         G_IMPLEMENT_INTERFACE (HLW_DBUS_TYPE_TICK_BOARD,
                                                hlw_tick_board_tick_board_iface_init))

static void
hlw_tick_board_handle_method_call (GDBusConnection *connection,
                                   const gchar *sender,
                                   const gchar *object_path,
                                   const gchar *interface_name,
                                   const gchar *method_name,
                                   GVariant *parameters,
                                   GDBusMethodInvocation *invocation,
                                   gpointer user_data)
{
  GDBusInterfaceSkeletonClass *skeleton_class;
  GDBusInterfaceVTable *skeleton_vtable;

  g_message ("%s: sender(%s), object_path(%s), interface_name(%s), method_name(%s)", G_STRFUNC,
      sender, object_path, interface_name, method_name);

  skeleton_class = G_DBUS_INTERFACE_SKELETON_CLASS (hlw_tick_board_parent_class);
  skeleton_vtable = skeleton_class->get_vtable (G_DBUS_INTERFACE_SKELETON (user_data));
  skeleton_vtable->method_call (connection,
                                sender,
                                object_path,
                                interface_name,
                                method_name,
                                parameters,
                                invocation,
                                user_data);
}

static GVariant *
hlw_tick_board_handle_get_property (GDBusConnection *connection,
                                    const gchar *sender,
                                    const gchar *object_path,
                                    const gchar *interface_name,
                                    const gchar *property_name,
                                    GError **error,
                                    gpointer user_data)
{
  GDBusInterfaceSkeletonClass *skeleton_class;
  GDBusInterfaceVTable *skeleton_vtable;

  skeleton_class = G_DBUS_INTERFACE_SKELETON_CLASS (hlw_tick_board_parent_class);
  skeleton_vtable = skeleton_class->get_vtable (G_DBUS_INTERFACE_SKELETON (user_data));

  return skeleton_vtable->get_property (connection,
                                        sender,
                                        object_path,
                                        interface_name,
                                        property_name,
                                        error,
                                        user_data);
}

static gboolean
hlw_tick_board_handle_set_property (GDBusConnection *connection,
                                    const gchar *sender,
                                    const gchar *object_path,
                                    const gchar *interface_name,
                                    const gchar *property_name,
                                    GVariant *variant,
                                    GError **error,
                                    gpointer user_data)
{
  GDBusInterfaceSkeletonClass *skeleton_class;
  GDBusInterfaceVTable *skeleton_vtable;

  skeleton_class = G_DBUS_INTERFACE_SKELETON_CLASS (hlw_tick_board_parent_class);
  skeleton_vtable = skeleton_class->get_vtable (G_DBUS_INTERFACE_SKELETON (user_data));
  return skeleton_vtable->set_property (connection,
                                        sender,
                                        object_path,
                                        interface_name,
                                        property_name,
                                        variant,
                                        error,
                                        user_data);
}

static const GDBusInterfaceVTable hlw_tick_board_vtable =
{
  hlw_tick_board_handle_method_call,
  hlw_tick_board_handle_get_property,
  hlw_tick_board_handle_set_property,
  {NULL}
};

static GDBusInterfaceVTable *
hlw_tick_board_get_vtable (GDBusInterfaceSkeleton *skeleton G_GNUC_UNUSED)
{
  return (GDBusInterfaceVTable *) &hlw_tick_board_vtable;
}

static void
hlw_tick_board_dispose (GObject *object)
{
  HlwTickBoard *self = HLW_TICK_BOARD (object);

  if (self->timeout_id != 0)
    {
      g_source_remove (self->timeout_id);
      g_source_unref (self->timeout_source);

      self->timeout_id = 0;
    }

  G_OBJECT_CLASS (hlw_tick_board_parent_class)->dispose (object);
}

static void
hlw_tick_board_class_init (HlwTickBoardClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GDBusInterfaceSkeletonClass *skeleton_class;

  object_class->dispose = hlw_tick_board_dispose;

  skeleton_class = G_DBUS_INTERFACE_SKELETON_CLASS (klass);
  skeleton_class->get_vtable = hlw_tick_board_get_vtable;
}

static void
hlw_tick_board_init (HlwTickBoard *self)
{
}

static gboolean
hlw_tick_board_tick_callback (gpointer user_data)
{
  gint new_tick, old_tick;
  HlwTickBoard *self = HLW_TICK_BOARD (user_data);

  old_tick = hlw_dbus_tick_board_get_current_tick (HLW_DBUS_TICK_BOARD (self));
  new_tick = old_tick + 1;

  hlw_dbus_tick_board_set_current_tick (HLW_DBUS_TICK_BOARD (self), new_tick);

  g_message ("%s: Tick (%d -> %d)", G_STRFUNC, old_tick, new_tick);

  return TRUE;
}

static gboolean
hlw_tick_board_handle_toggle_tick (HlwDBusTickBoard *object,
                                   GDBusMethodInvocation *invocation)
{
  HlwTickBoard *self = HLW_TICK_BOARD (object);

  if (self->timeout_id == 0)
    {
      g_message ("Start auto tick every 1 second.");

      self->timeout_source = g_timeout_source_new_seconds (1);

      g_source_set_callback (self->timeout_source, hlw_tick_board_tick_callback, self, NULL);
      self->timeout_id = g_source_attach (self->timeout_source, g_main_context_default ());

      g_source_unref (self->timeout_source);
 
    }
  else
    {
      g_message ("Stop auto-ticking.");

      g_source_remove (self->timeout_id);
      g_source_unref (self->timeout_source);

      self->timeout_id = 0;
    }

  hlw_dbus_tick_board_complete_toggle_tick (object, invocation);

  return TRUE;
}

static void
hlw_tick_board_tick_board_iface_init (HlwDBusTickBoardIface *iface)
{
  iface->handle_toggle_tick = hlw_tick_board_handle_toggle_tick;
}

HlwTickBoard *
hlw_tick_board_new (void)
{
  return g_object_new (HLW_TYPE_TICK_BOARD, NULL);
}
